FROM bellsoft/liberica-openjdk-debian:17

COPY training-backend-app.jar app.jar 
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]

